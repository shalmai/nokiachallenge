#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

"""
#Import libraries

import cv2 
import numpy as np

import json
import requests
import time
from datetime import timedelta
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import cv2
import urllib.request
import random

#--------------------------------------------------------------------#
#--Check for obstacles in the track and return true if obstacle detected
#--This function is called from image frame.
#--------------------------------------------------------------------#
def IsObstaclePresent(img):
    count = 0
    print("image shape:",img.shape)
#    vidcap = cv2.VideoCapture('pivideo3.mp4')
    f = 1
    x = int(200 / f)
    y = int(310 / f)
    width = int(180 / f)
    height = int(140 / f)
    count = 0
    
#    while(vidcap.isOpened()):
#        success, img = vidcap.read() 
        
    if(f > 1):
        width = int(img.shape[1] / f)
        height = int(img.shape[0] / f)
        dim = (width, height)
        img = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
            
#        print("------------------")
#        print("FRAME " + str(count))
#        if(not(success)):
#            print("STOPPED CAPTURE AT FRAME " + str(count))
#            break
        
    roi = img[y : y + height, x: x + width]
    
    roi = cv2.GaussianBlur(roi, (5, 5), 0)
    
    #Hough lines.
    gray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
    highThresh, threshIm = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    lowThresh = 0.5 * highThresh
    lowThresh = 128
    highThresh = 255
    edges = cv2.Canny(gray, lowThresh, highThresh, apertureSize = 3)
    lines = cv2.HoughLinesP(edges,rho = 1,theta = np.pi/180, threshold = 50, minLineLength = 50,maxLineGap = 50)
    
    if(lines is None):
        return False 
    
    nlines = 0
    for line in lines:
        x1, y1, x2, y2 = line[0]
        if(abs(y2 - y1) > 80):
            nlines = nlines + 1
            cv2.line(roi,(x1,y1),(x2,y2),(255,0,0),2)
            
##       #---Independent of previous operations - Contour Operations 
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    ret, im = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY_INV)

    vals = im
    vals = cv2.morphologyEx(vals, cv2.MORPH_OPEN, kernel)
    vals = cv2.morphologyEx(vals, cv2.MORPH_CLOSE, kernel)
    
    contours, hierarchy = cv2.findContours(vals, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#        
    ##throw away small contours.
    finalContourList = []
    isObstacleDetected = False
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if((area > 700 and area < (3000 / (f*f)))):
            if(len(lines) == 2):
                a0, b0, a1, b1 = lines[0][0]
                c0, d0, c1, d1 = lines[1][0]
                
                slope1 = np.arctan((b1 - b0) / (a1 - a0))
                slope1 = slope1 * 180.0 / np.pi
                if(slope1 < 0):
                    slope1 = slope1 + 360.0 
                
                slope2 = np.arctan((d1 - d0) / (c1 - c0))
                slope2 = slope2 * 180.0 / np.pi
                if(slope2 < 0):
                    slope2 = slope2 + 360.0
                
                isRails = False
                if((slope1 >= 45 and slope1 <= 90) or (slope1 >= 270 and slope1 <=300)):
                    if((slope2 >= 45 and slope2 <= 90) or (slope2 >= 270 and slope2 <=300)):
                        if(np.sqrt((b1 - b0) ** 2 + (a1 - a0) ** 2) > 100):
                            if(np.sqrt((d1 - d0) ** 2 + (c1 - c0) ** 2) > 100):
                                isRails = True

                if(isRails):
                    amid = (a0 + a1) / 2.0
                    bmid = (b0 + b1) / 2.0
                    cmid = (c0 + c1) / 2.0
                    dmid = (d0 + d1) / 2.0
    #                
                    M = cv2.moments(cnt)
                    cx = int(M['m10']/M['m00'])
                    cy = int(M['m01']/M['m00'])
                    if(cy <= 0.8 * roi.shape[0]):
                        if(cx >= amid and cx <= cmid):
                            if((cy <= b0) and (cy >= b1)):
                                if((cy >= d0) and (cy <= d1 - 30)):
#                                if((cy >= b0 and cy <= b1 - 30) and (cy >= d0 and cy <= d1 - 30)):
                                    cv2.circle(roi, (cx, cy), 10, (0, 0, 255))
                                    isObstacleDetected = True
                                    finalContourList.append(cnt)
                                    print("STOP TRAIN AT FRAME " + str(count))
                                    return isObstacleDetected
                        
        
#    if(isObstacleDetected):
#        roi = cv2.drawContours(roi, finalContourList, -1, (0,255,0), 3)
#        cv2.imwrite("opframes/hough" + str(count) + ".jpg", roi)
                       
    count = count + 1
    return isObstacleDetected


url_get_request_items = "http://192.168.0.100:5000/rest/items"
url_get_camera_stream = "http://192.168.0.190:8080/stream"
url_get_request_gyro = "http://192.168.0.180/gyro"
url_post_request_switch_track = "http://192.168.0.100:5000/rest/control/track_switch/x/y"
url_post_request_control_speed = "http://192.168.0.180/motor"
url_post_buzzer = "http://192.168.0.180/buzzer"

with open('tracking_the_train.json') as json_file:
    data_file_json = json.load(json_file)

#Get data from the url.
def GetDataItems():
    data_items = requests.request("GET", url_get_request_items, headers=None)
    return data_items.text

#Call this function to control the speed of train: ~700(pwm)
def ControlTrain(speed=700):
    params = {'x':speed}
    response = requests.request("GET", url_post_request_control_speed, params=params)
    print("Control Train", response)
    return response.text

#Switch track 
def SwitchTrack(ID, switch_required):
    data = {'ID':ID, 'values': switch_required}
    response = requests.request("POST", url_post_request_control_speed, data=data)
    print("Switch track", response)
    return response.text


def GetGyro():
    response = requests.request("GET", url_get_request_gyro, headers=None)
    response = requests.request("GET", "http://192.168.0.180/", headers=None)
    print("GetGyro", response)
    return response.text


def BuzzerControl(frequency):
    params={'x':frequency}
    response = requests.request("GET", url_post_buzzer, params=params)
    print("Buzzer Control", response)
    return response.text


def CameraStream():
    response = requests.request("GET", url_get_camera_stream)
    print("Camera Stream", response)
    return response

#Get position information of the train
def getTrackLocation(train_data):
  train_section = train_data["track"]["rail_sections"]
  #print(train_section)
  active_tracks = []
  for key, value in train_section.items():
    print(key, value)
    if(value["state"] == 0):
      active_tracks.append(key)
      print(active_tracks)
  return active_tracks

def main():
    vidcap = cv2.VideoCapture("http://192.168.0.190:8080/stream/video.jpeg")
    i = 0
    switch_positions = [33, 24]#33 and 24 are the switching positions on the
    while(vidcap.isOpened()):
      ControlTrain(speed=0)
      success,image = vidcap.read()
      avoid = IsObstaclePresent(image)
      print(i)
      cv2.imwrite("frame" + str(i) + ".jpeg", image)
      i = i + 1
      if(avoid):
        train_data = GetDataItems()
        gyro_data = json.loads(GetGyro())
        if(getTrackLocation(switch_positions[0] in json.loads(train_data)) and gyro_data["variable"]["direction"] == 0):
          print("trying to switch tracks")
          SwitchTrack(switch_positions[0], 1)
          ControlTrain(speed=900)
        elif(getTrackLocation(switch_positions[1] in json.loads(train_data)) and gyro_data["variable"]["direction"] == 1):
          print("trying to switch tracks")
          SwitchTrack(switch_positions[1], 1)
          ControlTrain(speed=900)
        else:
          print("trying to slow down")
          ControlTrain(speed=300)
          print("trying to blow buzzer")
          BuzzerControl(1000)

   
main()